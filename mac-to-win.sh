#!/bin/bash
# Script : mac-to-win
# @author : S.Lanteri - slanteri@exsymol.com
# @source : http://gitlab.exsymol.mc/others/mac-to-win
#
# -------------------------------------------------------------
# Détecte les noms de fichier non compatible avec un
# système Windows et remplace les caractères en 
# erreur par un ‘-‘
# -------------------------------------------------------------

# TODO :
# Mode test (sans modification)
# Calcul du nombre fichier detecté
# Test si fichier de destination existe ou pas
# Traiter d'abord les répertoires puis les fichiers

# chargement des options
source checker.cfg

clear
echo -e "\t - Script de détection d'erreurs de nom Windows v.$version -"
echo ""

path=''

while getopts "p:" option
    do
        case $option in
            p)
                if [[ -d $OPTARG ]]
                then
                    path=${OPTARG}
                elif [[ -f $OPTARG ]]
                then
                    echo "La valeur passée en argument correspond à un fichier et non un dossier"
                    exit 1
                else
                    echo "La valeur passée en argument n'est pas valide"
                    exit 1
                fi
                ;;
        esac
done

if [ -z "$path" ]
then
    read -p "Saisir un chemin d'accès : " path
fi

echo "Liste des dossiers & fichiers détectés dans $path : "
echo " "
echo "1 - Dossiers : "
find "$path" -depth -type d \( -name "* " -o -name "*[<>:\\|?*]*" \) -exec du -sh {} \;
echo " "
echo "2 - Fichiers : "
find "$path" -depth -type f \( -name "*[<>:\\|?*]*" -o -name "* .*" -o -name "* " \) -exec ls -l {} \;
echo " "
echo "Fin du traitement"