# Changelog
Tous les changements notables sur ce projet seront documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](http://keepachangelog.com/fr/1.0.0/)
et suis les recommandations du [Versionnage Sémantique](http://semver.org/lang/fr/spec/v2.0.0.html).

## [Non publié]

### Corrigé

- 

### Ajouté

- 

## [1.0.1] - 2018-10-08

### Corrigé

- Corrige l'erreur si le chemin d'accès comporte des espaces

### Ajouté

- Version du script affichée dans le script
- Améliorations cosmétiques

## [1.0.0] - 2018-09-20

- 1ere version en production du script
