# Détection des fichiers non compatibles avec les systèmes Windows

Ce script permet de détecter rapidement si le nom d'un dossier et/ou fichier est lisible sur un système Windows. 

Le script détecte la présence des caractères suivants : < > : \ | ? * ainsi que les dossiers & fichiers qui finissent par un espace blanc. Le script requiert que l'on passe un dossier en paramètre sinon il demandera un chemin d'accès durant le traitement.

/!\ Ne pas traiter un répertoire dont vous n'avez les accès

Ligne de commande  :

* -p : traite le répertoire passé en paramètre. ex. `./mac-to-win.sh -p "/Users/informatique/Desktop/Test"`

Le suivi des modifications se fait depuis le fichier CHANGELOG.

## Installation

1. Télécharger le fichier .sh depuis le dépot d'origine (http://gitlab.exsymol.mc/others/mac-to-win/blob/master/mac-to-win.sh)
1. Rendre exécutable le script *mac-to-win.sh* en utilisant `chmod +x mon-fichier`

## Exécution

Exécuter le script en saisissant : `./mac-to-win.sh -p /chemin/vers/mon/repertoire`
